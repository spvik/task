package com.example.tasktest.task.api;

import com.example.tasktest.common.exception.BadGatewayException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayDeque;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.*;

class TaskControllerTest {

    private final int THREADS = 10;
    private final int MAX_ATTEMPTS = 10;
    private final int DURATION = 10;

    private static final ConcurrentHashMap<String, ArrayDeque<LocalDateTime>> clients = new ConcurrentHashMap<>();


    @Test
    void methodOne() {
        Executor executor = Executors.newFixedThreadPool(THREADS);
        final Random random = new Random();
        for (int i = 1; i <= THREADS; i++) {
            executor.execute(new TestRequestRunnable(String.format("127.0.0.%d", i), random.nextInt(20)));
        }

    }

    class TestRequestRunnable implements Runnable {

        private String ipAddress;
        private int requests;


        TestRequestRunnable(String ipAddress, int requests) {
            this.ipAddress = ipAddress;
            this.requests = requests;
        }

        @Override
        public void run() {

            System.out.println(ipAddress + " request = " + requests);

            for (int i = 1; i <= requests; i++) {
                testRequest(ipAddress);
            }
        }

        private void testRequest(String address) {

            if (!clients.containsKey(address)) {
                var intervals = new ArrayDeque<LocalDateTime>(MAX_ATTEMPTS);
                clients.put(address, intervals);
            }

            var now = LocalDateTime.now();
            clients.get(address).offer(now);

            if (clients.get(address).size() >= MAX_ATTEMPTS) {
                if (clients.get(address).size() > MAX_ATTEMPTS) {
                    clients.get(address).poll();
                }
                var diff = ChronoUnit.MINUTES.between(Objects.requireNonNull(clients.get(address).peek()), now);
                if (diff <= DURATION) {
                    throw new BadGatewayException(ipAddress + " Request limit");
                }
            }
        }
    }
}