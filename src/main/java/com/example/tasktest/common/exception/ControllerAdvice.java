package com.example.tasktest.common.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(BadGatewayException.class)
    public ResponseEntity handleBadGatewayException(BadGatewayException e) {
        return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
    }
}
