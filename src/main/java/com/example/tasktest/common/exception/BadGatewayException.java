package com.example.tasktest.common.exception;

import org.springframework.http.HttpStatus;

public class BadGatewayException extends RuntimeException {

    private HttpStatus httpStatus = HttpStatus.BAD_GATEWAY;

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public BadGatewayException(String message) {
        super(message);
    }
}