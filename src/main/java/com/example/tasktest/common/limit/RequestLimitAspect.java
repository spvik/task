package com.example.tasktest.common.limit;

import com.example.tasktest.common.exception.BadGatewayException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayDeque;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Aspect
@Component
public class RequestLimitAspect {

    @Value("${app.max-attempts}")
    Integer MAX_ATTEMPTS = 10;

    @Value("${app.duration}")
    Integer DURATION = 10;

    private static final ConcurrentHashMap<String, ArrayDeque<LocalDateTime>> clients = new ConcurrentHashMap<>();

    @Before("@annotation(RequestLimit)")
    public void requestLimitation(JoinPoint joinPoint) throws BadGatewayException, Throwable {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        var address = request.getRemoteAddr();

        if (!clients.containsKey(address)) {
            var intervals = new ArrayDeque<LocalDateTime>(MAX_ATTEMPTS);
            clients.put(address, intervals);
        }

        var now = LocalDateTime.now();
        clients.get(address).offer(now);

        if (clients.get(address).size() >= MAX_ATTEMPTS) {
            if (clients.get(address).size() > MAX_ATTEMPTS) {
                clients.get(address).poll();
            }
            var diff = ChronoUnit.MINUTES.between(Objects.requireNonNull(clients.get(address).peek()), now);
            if (diff <= DURATION) {
                throw new BadGatewayException("Request limit");
            }
        }
    }
}
