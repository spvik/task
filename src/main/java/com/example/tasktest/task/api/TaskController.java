package com.example.tasktest.task.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TaskController {

    @GetMapping("/method-one")
    public ResponseEntity methodOne() {
        return ResponseEntity.ok("");
    }

}
