FROM openjdk:11
ADD ./build/libs/task-test-0.0.1-SNAPSHOT.jar target/api.jar
CMD ["java","-jar", "target/api.jar"]